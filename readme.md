# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

Práctica para entender el uso de S3 para subir ficheros y compartirlos públicamente


# Instalación
-----------------------


Ejecuta:
```
$ vagrant up
```

Entra en el host ejecutando:
```
vagrant ssh
```

Configura tus claves con acceso a S3 con los siguientes pasos:

- Crea un fichero en `~/.aws/credentials` con el siguiente contenido

```
[default]
aws_access_key_id=TU_ACCESS_KEY
aws_secret_access_key=TU_SECRET_KEY
```

Donde `TU_ACCESS_KEY` y `TU_SECRET_KEY` son credenciales que ya tendrás de crear una cuenta en AWS.


# Instrucciones
-----------------------

- En la máquina virtual, entra en `src/` y ejecuta `composer update
- Abre el fichero `upload_file_s3.php`
- Allí hay dos secciones a rellenar:
- La primera sección es rellenar la variable `$YOUR_BUCKET_NAME` con el bucket con el que quieras hacer la práctica (puede tener cualquier nombre)
- En la segunda sección tienes que completar el código que falta para subir el fichero `wallpaper1.jpg` al bucket especificado a S3
- Puedes ir haciendo pruebas ejecutando `php upload_file_s3.php` y deberías ver la URL pública de la imágen como resultado si todo ha ido bien


# Desinstalación
-----------------------

```
$ vagrant destroy
```

- Borra la imagen subida a S3 manualmente entrando en la consola de AWS (https://console.aws.amazon.com/s3/home?region=eu-west-1)