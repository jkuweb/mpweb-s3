<?php

require_once('vendor/autoload.php');

use Aws\S3\S3Client;
use Aws\S3\Exception\PermanentRedirectException;
use Aws\S3\Exception\S3Exception;
use Aws\Exception\CredentialsException;


$s3 = new S3Client([
	'version' => 'latest',
	'region'  => 'eu-west-1',
]);

try {
	$result = $s3->listBuckets();	
} catch (CredentialsException $e) {
	echo "Error! Your credentials are invalid. Have you created the file ~/.aws/credentials ?" . PHP_EOL;
	exit();
}


foreach ($result['Buckets'] as $bucket) {
	echo 'Bucket name: ' . $bucket['Name'] . PHP_EOL;
}



$FILENAME = 'wallppaer1.jpg';




// EDIT THIS VARIABLE TO MATCH YOUR BUCKET NAME
$YOUR_BUCKET_NAME = 'replace-me';
// END EDIT


// EDIT THE FOLLOWING SECTION TO UPLOAD THE wallpaper1.jpg FILE TO YOUR S3 BUCKET
$s3-> ...
// END EDIT SECTION







try {
	$result = $s3->getObject([
		'Bucket' => $YOUR_BUCKET_NAME,
		'Key'    => $FILENAME,
	]);
} catch (PermanentRedirectException $e) {
	echo PHP_EOL . "Error! wallpaper1.jpg has not been found in the bucket!" . PHP_EOL;
} catch (S3Exception $e) {
	echo PHP_EOL . 'Error! It seems that the bucket you have entered does not exists. Have you adjusted $YOUR_BUCKET_NAME variable?' . PHP_EOL;
}


if (empty($result['@metadata']['effectiveUri'])) {
	echo PHP_EOL . "Error! wallpaper1.jpg has been uploaded but it seems that it does not have a public URL!" . PHP_EOL;	
	echo "Adjust the 'Static Website Hosting' of the bucket $YOUR_BUCKET_NAME accordingly to allow access from Internet " . PHP_EOL;	
	exit();
}

echo PHP_EOL . "Success! wallpaper1.jpg has been found in the bucket!" . PHP_EOL;
echo "Public URL: " . $result['@metadata']['effectiveUri'] . PHP_EOL;